#!/bin/bash
while true; do
    case "$1" in
        -c)
            CONTINUOUS=true
	    shift
            ;;
    	--level-name)
	    LEVEL_NAME="$2"
	    shift
	    ;;
	*)
	    break
	    ;;
    esac
done

doSync() {
    for DIMENSION in ${LEVEL_NAME}{,_nether,_the_end}; do
        if [ -d /dev/shm/$(hostname)/${DIMENSION} ]; then
	    echo "syncing ${DIMENSION}"
            rsync --archive --delete /dev/shm/$(hostname)/${DIMENSION} /var/lib/minecraft/worldstore/${DOMENSION}
        fi
    done
}

doShutdown() {
  echo -e "Shutdown sync process"
  if [ ! -z ${SLEEP_PID} ]; then
    kill -15 ${SLEEP_PID}
    wait ${SLEEP_PID}
    exit 0
  fi
}

trap 'doShutdown' SIGTERM

if [ -z "$LEVEL_NAME" ]; then
    LEVEL_NAME=$(grep ^level-name server.properties | cut -d= -f2)
else
    if [ ${CONTINUOUS:-false} == true ]; then
        while true; do
            doSync
            sleep ${MC_SYNC_INTERVAL:-5m} &
	    SLEEP_PID=$!
	    wait $!
        done
    else
        doSync
    fi

fi








